import express from 'express';
import cors from 'cors';
import _ from 'lodash';
import colorParser from 'parse-color';

const app = express();
app.use(cors());

const INVALID_FULLNAME = 'Invalid fullname';
const INVALID_USERNAME = 'Invalid username';

app.get('/task2A', (req, res) => {
	const sum = (+req.query.a || 0) + (+req.query.b || 0);
	res.send(sum.toString());
});

app.get('/task2B', (req, res) => {
    const fullname = decodeURIComponent(req.query.fullname);
	res.send(parseFullname(fullname));
});

function parseFullname(fullname) {
    if (!fullname || /\d|_|\//.test(fullname)) {
        return INVALID_FULLNAME;
    }
    // remove all unnecessary spaces
    fullname = fullname.trim().replace(/\s+/g, ' ');

    let initials = fullname.split(' ');
    initials = changeRegister(initials);

    switch (initials.length) {
        case 1:
            return initials[0];
        case 2:
            return `${initials[1]} ${initials[0][0]}.`;
        case 3:
            return `${initials[2]} ${initials[0][0]}. ${initials[1][0]}.`;
        default: 
            return INVALID_FULLNAME;
    }
}

function changeRegister(values) {
	return values.map(function(value) {
		value = value.toLowerCase();
		return value[0].toUpperCase() + value.slice(1);
	});
}

app.get('/task2C', (req, res) => {
	const requestedName = req.query.username;
	if (!requestedName) {
        return INVALID_USERNAME;
    }

    // let user = url.username.match(/(?:http[s]?:\/\/)?(?:www.)?(?:[\w-]+\.[\w-]+\/)?@?(\w+\.?\w+)/);
    const filterUsername = /(?:http[s]?:\/\/)?(?:www.)?(?:[\w-]+\.[\w-]+\/)?@?(\w+\.?\w+)/;
    const username = requestedName.match(filterUsername)[1];
  
  	res.send(`@${username}`);
});


app.get('/task2D', (req, res) => {
  let color = ('' + req.query.color).trim();
  color = color.replace(/%20/g, ' ').replace(/ /g, '');

  let result;
  try {
    result = parseColor(color)
    if (result == undefined) {
      throw new Error();
    }
  } catch (err) {
    result = 'Invalid color';
  }
  res.send(result);
});

function parseColor(color) {
  let result;
  if (/#([a-f0-9]{3}){1,2}\b/i.test(color)) {
    result = colorParser(color).hex;
  } else if (/^rgb\((\d+),(\d+),(\d+)\)$/i.test(color)) {
    const parsed = colorParser(color);
    if (parsed.rgb[0] <= 255 && parsed.rgb[1] <= 255 && parsed.rgb[2] < 255) {
      result = parsed.hex;
    }
  } else if (/^hsl\((\d+),(\d+)%,(\d+)%\)$/i.test(color)) {
    const parsed = colorParser(color);
    if (parsed.hsl[1] <= 100 && parsed.hsl[2] <= 100) {
      result = parsed .hex;
    }
  } else if (color.length > 2 && color.length < 7){
    result = colorParser(`#${color}`).hex;
  }
  return result;
}

app.listen(3000, () => {
  console.log('App listening on port 3000!');
});