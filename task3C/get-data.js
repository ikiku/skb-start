import express from 'express';
import fetch from 'isomorphic-fetch';
import Promise from 'bluebird';
import fs from 'fs';


async function lazyLoadData() {
  if (!pokemons) {
    const pokemonsInfo = await getPokemons(pokemonUrl);
    const pokemonPromises = pokemonsInfo.map(info => {
      return getPokemon(info.url);
    });
    const pokemonsFull = await Promise.all(pokemonPromises);
    return pokemonsFull.map(pokemon => {
      const result = _.pick(pokemon, FIELDS_OF_POKEMON);
      // for more quick testing
      fs.writeFile("/pokemones.json", JSON.stringify(result), function(err) {
          if(err) console.log(err);
      });
      return result;
    });
  }
  return pokemons;
}

async function getPokemons(url) {
  const response = await fetch(url);
  const page = await response.json();
  const pokemons = page.results;
  
  if (page.next) {
    const nextPokemons = await getPokemons(page.next);
    return [ ...pokemons, ...nextPokemons ];
  }
  return pokemons;
}

async function getPokemon(url) {
  const response = await fetch(url);
  const pokemon = await response.json();
  return pokemon;
}

export default lazyLoadData;