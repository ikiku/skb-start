import express from 'express';
import cors from 'cors';
import fetch from 'isomorphic-fetch';
import _ from 'lodash';
import Promise from 'bluebird';
import fs from 'fs';

import loadData from './get-data'

const router = express.Router();
const app = express();

app.use(cors());
app.use('/', router)
app.listen(3000, () => console.log('App listening on port 3000!'));



const baseUrl = 'https://pokeapi.co/api/v2';
const pokemonUrl = `${baseUrl}/pokemon/`;

const __DEV__ = true;
const FIELDS_OF_POKEMON = ['id', 'name', 'height', 'weight'];
const LIMIT = 20;
const OFFSET = 0;

let pokemons;


router.use(async (req, res, next) => {
  req.offset = +req.query.offset || OFFSET;
  req.limit = +req.query.limit || LIMIT;
  if (__DEV__) {
    pokemons = fakeLoadData();
  } else {
    pokemons = await loadData();
  }
  next();
});

function fakeLoadData() {
  return JSON.parse(fs.readFileSync('/pokemons.json', 'utf8'));
}

router.get('/', (req, res) => {
  const sortPokemons = _.sortBy(pokemons, pokemon => pokemon.name);
  const limitedPokemons = limitAndOffset(sortPokemons, req.limit, req.offset); 
  const names = limitedPokemons.map(pokemon => { return pokemon.name; });
  return res.send(names);
});

router.get('/angular', (req, res) => {
  const pokemonsWithAngular = angular(pokemons);
  const sortPokemons = pokemons.sort(comporatorMin('angular'));
  const limitedPokemons = limitAndOffset(sortPokemons, req.limit, req.offset); 
  const names = limitedPokemons.map(pokemon => { return pokemon.name; });
  return res.send(names);
});

function angular() {
  const pokemonsWithAngular = pokemons.map(pokemon => {
    pokemon.angular = pokemon.weight / pokemon.height; 
    return pokemon;
  });
  return pokemonsWithAngular;
}

router.get('/huge', (req, res) => {
  const sortPokemons = pokemons.sort(comporatorMax('height'));
  const limitedPokemons = limitAndOffset(sortPokemons, req.limit, req.offset); 
  const names = limitedPokemons.map(pokemon => { return pokemon.name; });
  return res.send(names);
});

router.get('/micro', (req, res) => {
  const sortPokemons = pokemons.sort(comporatorMin('height'));
  const limitedPokemons = limitAndOffset(sortPokemons, req.limit, req.offset); 
  const names = limitedPokemons.map(pokemon => { return pokemon.name; });
  return res.send(names);
});

router.get('/fat', (req, res) => {
  const pokemonsWithAngular = angular(pokemons);
  const sortPokemons = pokemonsWithAngular.sort(comporatorMax('angular'));
  const limitedPokemons = limitAndOffset(sortPokemons, req.limit, req.offset); 
  const names = limitedPokemons.map(pokemon => { return pokemon.name; });
  return res.send(names);
});

router.get('/heavy', (req, res) => {
  const sortPokemons = pokemons.sort(comporatorMax('weight'));
  const limitedPokemons = limitAndOffset(sortPokemons, req.limit, req.offset); 
  const names = limitedPokemons.map(pokemon => { return pokemon.name; });
  return res.send(names);
});

router.get('/light', (req, res) => {
  const sortPokemons = pokemons.sort(comporatorMin('weight'));
  const limitedPokemons = limitAndOffset(sortPokemons, req.limit, req.offset); 
  const names = limitedPokemons.map(pokemon => { return pokemon.name; });
  return res.send(names);
});



function comporatorMax(key) {
  return function (a, b) {
    const result = b[key] - a[key];
    return result == 0 ? sortByName(a, b) : result;
  }
}

function comporatorMin(key) {
  return function (a, b) {
    const result = a[key] - b[key];
    return result == 0 ? sortByName(a, b) : result;
  }
}

function sortByName (a, b) {
  return (a.name < b.name) ? -1 : (a.name > b.name) ? 1 : 0;
}

function limitAndOffset(pokemons, limit, offset) {
  const dropedPokemons = _.drop(pokemons, offset);
  return _.take(dropedPokemons, limit)
}