import express from 'express';
import cors from 'cors';
import fetch from 'isomorphic-fetch';
import _ from 'lodash';

const router = express.Router();
const app = express();

app.use(cors());
app.use('/', router)
app.listen(3000, () => console.log('App listening on port 3000!'));

const petsUrl = 'https://gist.githubusercontent.com/isuvorov/55f38b82ce263836dadc0503845db4da/raw/pets.json';

let data;
fetch(petsUrl)
.then(async(url) => {
  data = await url.json()
})
.catch(err => console.log('Data is not loaded: ', err));



router.use((req, res, next) => {
  req.pets = data.pets;
  req.users = data.users;
  next()
});
 
router.get('/', (req, res) => {
  res.send(data);
});

function clone(structure) {
    return JSON.parse(JSON.stringify(structure));
}

function sort(array) {
  return array.sort((a, b) => a.id - b.id)
}



router.use('/users', (req, res, next) => {
  const typePet = req.query.havePet;
  if (typePet) {
    const choosenPets = req.pets.filter(p => p.type == typePet);
    const userIdsWithPet = choosenPets.map(pet => {
      return pet.userId;
    });
    req.users = req.users.filter(u => _.includes(userIdsWithPet, u.id));
  }
  next();
});

router.get('/users', (req, res) => {
  res.send(sort(req.users));
});

router.get('/users/populate', (req, res) => {
  let users = clone(req.users);
  users.forEach(user => populateUser(user, req.pets));
  res.send(sort(users));
});

function populateUser(user, allPets) {
  user.pets = allPets.filter(pet => user.id == pet.userId);
}

router.get('/users/:value/populate', (req, res) => {
  const user = findUserByParam(req.params.value, req.users);
  if (_.isUndefined(user)) {
    res.status(404).send('Not Found');
  } else {
    let cloneUser = clone(user);
    populateUser(cloneUser, req.pets);
    res.send(cloneUser);
  }
});

router.get('/users/:value', (req, res) => {
  const user = findUserByParam(req.params.value, req.users);
  if (_.isUndefined(user)) {
    res.status(404).send('Not Found');
  } else {
    res.send(user);
  }
});

function findUserByParam(value, users) {
  let param;
  if (_.isInteger(+value)) {
    param = 'id';
  } else if (_.isString(value)) {
    param = 'username';
  }
  return users.filter(u => u[param] == value)[0];
}


router.use('/pets', (req, res, next) => {
  if (req.query.type) {
    req.pets = req.pets.filter(p => p.type == req.query.type);
  }
  if (req.query.age_gt) {
    req.pets = req.pets.filter(p => p.age > +req.query.age_gt)
  }
  if (req.query.age_lt) {
    req.pets = req.pets.filter(p => p.age < +req.query.age_lt)
  }
  next()
})

router.get('/pets', (req, res) => {
  res.send(sort(req.pets));
});

router.get('/pets/populate', (req, res) => {
  let pets = clone(req.pets);
  pets.forEach(pet => populatePet(pet, req.users));
  res.send(sort(pets));
})

function populatePet(pet, users) {
  pet.user = users.filter(user => user.id == pet.userId)[0];
}

router.get('/pets/:id', (req, res) => {
  const pet = data.pets.filter(p => p.id == req.params.id)[0];
  if (_.isUndefined(pet)) {
    res.status(404).send('Not Found');
  } else {
    res.send(pet);
  }
});

router.get('/pets/:id/populate', (req, res) => {
  const pet = clone(req.pets.filter(pet => pet.id == req.params.id)[0]);
  populatePet(pet, req.users);
  res.send(pet);
})
