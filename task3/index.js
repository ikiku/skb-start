import express from 'express';
import cors from 'cors';
import fetch from 'isomorphic-fetch';
import _ from 'lodash';

const app = express();
app.use(cors());
app.listen(3000, () => console.log('App listening on port 3000!'));

const pcUrl = 'https://gist.githubusercontent.com/isuvorov/ce6b8d87983611482aac89f6d7bc0037/raw/pc.json';

let pc;
fetch(pcUrl)
.then(async(url) => {
  pc = await url.json()
})
.catch(err => console.log('Data is not loaded: ', err));



app.get('/volumes', (req, res) => {
  let volumes = {};
  pc.hdd.forEach((hdd) => {
    volumes[hdd.volume] = (volumes[hdd.volume] || 0) + hdd.size;
  });

  volumes = _.mapValues(volumes, (size) => size + 'B');
  res.json(volumes);
});



app.get('/*', (req, res) => {
  const url = req.originalUrl;
  const path = _.trim(url, '/').split('/');

  try {
    const result = (path == '') ? pc : searchData(pc, path); 
    res.json(result);
  } catch (err) {
    res.status(404).send('Not Found');
  }
});

function searchData(data, structure) {
  const current = structure.shift();
  if (_.isUndefined(data) || _.isUndefined(data[current])) {
    throw new Error();
  } else if (structure.length == 0) {
    if (_.isArray(data) && !isNaN(current)) {
      return data[current];
    } else if (_.isPlainObject(data) && {}.hasOwnProperty.call(data, current)) {
      return data[current];
    }
    throw new Error();
  } 
  return searchData(data[current], structure)
}